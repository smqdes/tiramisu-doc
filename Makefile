SRC=$(wildcard *.txt)
HTMLFRAGMENT=$(addsuffix .html, $(basename $(SRC)))

.SUFFIXES:

.PHONY: all clean

all: html

html: $(HTMLFRAGMENT)

%.html: %.txt
	rst2html --stylesheet ./style.css $< > ./$@

#clean:
#	make -C ./build clean

