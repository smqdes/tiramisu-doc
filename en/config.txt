The :class:`Config`
====================

Tiramisu is made of almost three main objects :

- :class:`Option` stands for the option types
- :class:`OptionDescription` is the shema, the option's structure
- :class:`Config` which is the whole configuration entry point

The handling of options
~~~~~~~~~~~~~~~~~~~~~~~~~~

The handling of options is split into two parts: the description of
which options are available, what their possible values and defaults are
and how they are organized into a tree. A specific choice of options is
bundled into a configuration object which has a reference to its option
description (and therefore makes sure that the configuration values
adhere to the option description).


.. currentmodule:: hello

.. function:: make_config

    First, let's make a :class:`Config`.

    Option objects can be created in different ways. Let's perform very basic
    :class:`Config` manipulations with an Option Description name `optgroup`.

.. literalinclude:: ../src/hello.py
   :pyobject: make_config

.. function:: create_config

    Then we instanciate the :class:`Config` object.

.. literalinclude:: ../src/hello.py
   :pyobject: create_config

:src:`hello`

The :class:`Option` (in this case the :class:`BoolOption`),
are organized into a tree into nested
:class:`~tiramisu.option.OptionDescription` objects.

.. image:: config.png

Every option has a name, as does every option group.

.. _read_write:

The read/write properties on the :class:`Config`
------------------------------------------------

Well, once the configuration's structure is set and the :class:`Config` object is instanciated,

we can access a bunch of convenience properties methods
like setting the `read_write` attribute :

.. currentmodule:: property

.. function:: create_config

.. literalinclude:: ../src/property.py
  :pyobject: create_config
  :linenos:
  :emphasize-lines: 4, 6

:src:`property`

MetaConfigs
~~~~~~~~~~~~~~~~~~~~~~~~~~
MetaConfigs are configs that will contain other configs or MetaConfigs. Those configs
will have the same options as the MetaConfig, but will be differentiated by a session id.

.. function:: create_metaconfig

.. literalinclude:: ../src/property.py
  :lines: 34-50
  :linenos:

:src:`property`

.. note::
  You can also put a MetaConfig inside another MetaConfig, by creating it and putting it 
  in the list when creating the second MetaConfig.

.. function:: meta_in_meta

.. literalinclude:: ../src/property.py
  :lines: 70-78
  :linenos:

:src:`property`

You can also create your configs directly into your MetaConfig with the "new" instruction.

.. function:: create_metaconfig2

.. literalinclude:: ../src/property.py
  :lines: 51-57
  :linenos:

:src:`property`

.. note::
  As you use "meta_cfg.config.new('new_session_id')" to add a new config in your MetaConfig, 
  you can use "meta_cfg.config.pop('session_id')"to remove one.



You can get a list of every configs your MetaConfig contains by using the 
"meta_cfg.config.list()" command. This will give you a generator object that you can iterate
onto.

.. function:: list_configs

.. literalinclude:: ../src/property.py
  :lines: 59-68
  :linenos:

:src:`property`

