Storage
=======

.. automodule:: tiramisu.storage

.. automethod:: tiramisu.storage.default_storage

.. image:: storage.png

Dictionary
~~~~~~~~~~

.. automodule:: tiramisu.storage.dictionary


Sqlite3
~~~~~~~

.. automodule:: tiramisu.storage.sqlite3


Example
~~~~~~~

>>> from tiramisu.option import StrOption, OptionDescription, Config, default_storage,Storage
>>> default_storage.settings(engine='sqlite3', dir_database='/tmp/tiramisu')
>>> s = StrOption('str', '')
>>> o = OptionDescription('od', '', [s])
>>> c1 = Config(o, persistent=True, session_id='session')
>>> print(c1.option('str').value.get())
>>> c1.option('str').value.set('yes')
>>> print(c1.option('str').value.get())
'yes'
>>> del(c1)
>>> c2 = Config(o, persistent=True, session_id='session')
>>> print(c2.option('str').value.get())
'yes'
>>> del(c2)
>>> list_sessions()
['session']
>>> delete_session('xxxx')
>>> c3 = Config(o, persistent=True, session_id='session')
>>> print(c3.option('str').value.get())

>>> storage = Storage(engine='dictionary')