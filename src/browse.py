from property import create_config

cfg = create_config()

def get_option_by_path():
    print(cfg.option('od1.var1'))
    # <tiramisu.api.TiramisuOption object at 0x7f3876cc5940>
    print(cfg.option('od1.var1').option.name())
    # 'var1'
    print(cfg.option('od1.var1').option.doc())
    # 'first option'
    return

def browse_config():
    # get all options
    print(cfg.option.value.dict())
    # {'var1': None, 'var2': 'value'}

    # get the `od1` option description
    print(cfg.option('od1').value.dict())
    # {'od1.var1': None, 'od1.var2': 'value'}

    # get to the var1 option's value
    print(cfg.option('od1.var1').value.get())
    # None

    # get to the var2 option's default value
    print(cfg.option('od1.var2').value.get())
    # 'value'

    try:
        cfg.option('od1.idontexist').value.get()
    except:
        pass
        # raises an error
    return

def make_dict():
    # get the `od1` option description
    print(cfg.option('od1').valu.dict('', '' ,'', '', True))
    #{'od1.var1': 'éééé', 'od1.var2': 'value'}
    print(cfg.option('od1').value.dict('', '' ,'', '', True))
    # {'var1': 'éééé', 'var2': 'value'}

def modify_config():
    # changing the `od1.var1` value
    cfg.option('od1.var1').value.set('éééé')
    print(cfg.option('od1.var1').value.get())
    # 'éééé'

    # carefull to the type of the value to be set
    try:
        cfg.option('od1.var1').value.set(23454)
    except:
        pass
        # raises a ValueError
    # Traceback (most recent call last):
    # [...]
    # ValueError
    #
    # During handling of the above exception, another exception occurred:
    #
    # Traceback (most recent call last):
    # [...]
    # ValueError: "23454" is an invalid string for "first variable"

    # let's come back to the default value
    cfg.option('od1.var2').value.reset()
    print(cfg.option('od1.var2').value.get())
    # 'value'


if __name__ == '__main__':
    get_option_by_path()
    browse_config()
    modify_config()
    make_dict()
