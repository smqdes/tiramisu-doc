from tiramisu import Config, OptionDescription, IntOption, Params, ParamOption

def callback_synthax():
        opt = OptionType('name', 'doc', callback=function_name, 
                                        callback_params=Params(ParamOption(first_param), ParamOption(second_param), ...))

def double_value(value: int) :
        return value*2

def callback_example():
        opt1 = IntOption('opt1', '', default=4)
        opt2 = IntOption('opt2', '', callback=double_value, 
                                callback_params=Params(ParamOption(opt1)))
        rootod = OptionDescription('root', '', [opt1, opt2])
        cfg = Config(rootod)

        # We want opt2 to have the double of opt1's value
        print(cfg.option('opt2').value.get())
        #>>> 8
        # The callback is working !
        # But what if we change the value of op1 ?
        cfg.option('opt1').value.set(5)
        print(cfg.option('opt2').value.get())
        #>>> 10
        # The value of opt2 changed too !

def set_after_callback():
        opt1 = IntOption('opt1', '', default=4)
        opt2 = IntOption('opt2', '', callback=double_value, 
                                callback_params=Params(ParamOption(opt1)))
        rootod = OptionDescription('root', '', [opt1, opt2])
        cfg = Config(rootod)

        # First let's use the callback funtion
        print(cfg.option('opt2').value.get())
        #>>> 8
        cfg.option('opt2').value.set(50)
        print(cfg.option('opt2').value.get())
        # >>> 50
        # opt2's value is set, but what if we change opt1'value ?
        cfg.option('opt1').value.set(5)
        print(cfg.option('opt2').value.get())
        # >>> 50
        # opt2's value didn't change, the callback hasn't been called

set_after_callback()