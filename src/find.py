"this is only to make sure that the tiramisu library loads properly"

from tiramisu import Config
from tiramisu import OptionDescription, StrOption
from tiramisu.setting import undefined

def make_config():
    var1 = StrOption('var1', '')
    var2 = StrOption('var2', '')
    var3 = StrOption('var3', '')
    od1 = OptionDescription('od1', '', [var1, var2, var3])
    var4 = StrOption('var4', '')
    var5 = StrOption('var5', '')
    var6 = StrOption('var6', '')
    var7 = StrOption('var1', '', u'value')
    od2 = OptionDescription('od2', '', [var4, var5, var6, var7])
    rootod = OptionDescription('rootod', '', [od1, od2])
    return Config(rootod)

def find_in_config():
    cfg = make_config()
    print(cfg.option.find(name='var1'))
    # [<tiramisu.api.TiramisuOption object at 0x7f490a530f98>, <tiramisu.api.TiramisuOption object at 0x7f490a530748>]

    # If the option name is unique, the search can be stopped once one matched option
    # has been found:
    print(cfg.option.find(name='var1', first=True))
    # <tiramisu.api.TiramisuOption object at 0x7f6c2beae128>

    # a search object behaves like a cfg object, for example
    print(cfg.option.find(name='var1', first=True).option.name())
    # 'var1'
    print(cfg.option.find(name='var1', first=True).option.doc())

    # a search can be made with various criteria
    print(cfg.option.find(name='var3', value=undefined))
    print(cfg.option.find(name='var3', type=StrOption))

    # the find method can be used in subconfigs
    print(cfg.option('od2').find('var1'))


if __name__ == "__main__":
    find_in_config()
