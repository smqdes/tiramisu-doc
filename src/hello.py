"this is only to make sure that the tiramisu library loads properly"

from tiramisu import Config
from tiramisu import OptionDescription, BoolOption

def make_config():
    # let's create a group of options
    descr = OptionDescription("optgroup", "", [
        # ... with only one option inside
        BoolOption("bool", "", default=False)])
    return Config(descr)

def create_config():
    cfg = make_config()
    print(cfg.help())
    # returns the whole help on the config
    print(cfg.option.help())
    # returns the whole help on the options
    return cfg

if __name__ == "__main__":
    cfg = create_config()
    print(cfg)
    # returns something like <tiramisu.api.Config object at 0x7f60351debc8>
