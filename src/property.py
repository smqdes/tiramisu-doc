from tiramisu import Config, MetaConfig
from tiramisu import StrOption, OptionDescription

def make_config():
    # let's declare some options
    var1 = StrOption('var1', 'first option')
    # an option with a default value
    var2 = StrOption('var2', 'second option', default='value')
    # let's create a group of options
    od1 = OptionDescription('od1', 'first OD', [var1, var2])

    # let's create another group of options
    rootod = OptionDescription('rootod', '', [od1])
    # print(rootod)
    # <tiramisu.option.optiondescription.OptionDescription object at 0x7fa76389d780>
    # let's create the config
    return Config(rootod)

def create_config():
    cfg = Config()
    # the api is read only
    cfg.property.read_only()
    # the read_write api is available
    cfg.property.read_write()
    return cfg

'''''
if __name__ == "__main__":
    cfg = create_config()
    print(cfg)
    # returns something like <tiramisu.api.Config object at 0x7f60351debc8>
'''''

def create_metaconfig():
    opt1 = StrOption('opt1', '', default='opt1')
    opt2 = StrOption('opt2', '', default='opt2')
    cfg1 = Config('cfg1')
    cfg2 = Config('cfg2')
    rootod = OptionDescription('root', '', [opt1, opt2])
    meta_cfg = MetaConfig([cfg1, cfg2], optiondescription=rootod, session_id='meta')
    #We now have 2 configs in our MetaConfig
    meta_cfg.config('cfg1').option('opt1').value.set('new_value')
    print(meta_cfg.config('cfg1').option('opt1').value.get())
    # new_value
    print(meta_cfg.config('cfg2').option('opt1').value.get())
    # opt1
    # cfg1 and cfg2 have the same options, with the same names and properties, but can be
    # used separately. It's like having different versions of a Config, each version having
    # different option values.

def create_metaconfig2():
    opt1 = StrOption('opt1', '', default='opt1')
    opt2 = StrOption('opt2', '', default='opt2')
    rootod = OptionDescription('root', '', [opt1, opt2])
    meta_cfg = MetaConfig([], optiondescription=rootod, session_id='meta')
    cfg1 = meta_cfg.config.new('cfg1')
    cfg2 = meta_cfg.config.new('cfg2')

def list_configs():
    opt1 = StrOption('opt1', '', default='opt1')
    opt2 = StrOption('opt2', '', default='opt2')
    rootod = OptionDescription('root', '', [opt1, opt2])
    meta_cfg = MetaConfig([], optiondescription=rootod, session_id='meta')
    cfg1 = meta_cfg.config.new('cfg1')
    cfg2 = meta_cfg.config.new('cfg2')

    for cfg in meta_cfg.config.list():
        print(cfg.config.name())

def meta_in_meta():
    opt1 = StrOption('opt1', '', default='opt1')
    opt2 = StrOption('opt2', '', default='opt2')
    cfg1 = Config('cfg1')
    cfg2 = Config('cfg2')
    od1 = OptionDescription('od1', '', [opt1])
    od2 = OptionDescription('od2', '', [opt2])
    meta_cfg1 = MetaConfig([cfg1], optiondescription=od1, session_id='meta1')
    meta_cfg2 = MetaConfig([cfg2, meta_cfg1], optiondescription=od2, session_id='meta2')
